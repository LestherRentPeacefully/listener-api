
/*
* Environment Variable to be exported
* */

export const environment = {
    listenerTimer:13*1000,
    mainAppURL: 'http://rentpeacefully.com',
    firebaseConfig: { /* Firebase config */
        credential: require("../../serviceAccountKey.json"),
        databaseURL: "https://blockchain-projects.firebaseio.com"
    },
    smtpConfig:{    /* Nodemailer config */
        host:'mail.rentpeacefully.com',
        port:587,
        secure:false,
        auth:{
            user:'noreply@rentpeacefully.com',
            pass:'Rent4$'
        }
    },
    altcoinNodesAuth: {
        user: 'user',
        password: 'password'
    },
    blockchain:{
        ETH:{
            nodeURL: 'http://localhost:8545',
            // nodeURL: 'http://164.68.106.226:8545',
            decimals: 18,
            minConfirm: 12,
            listenerTimer:13*1000,
            securePlatformWallet:{
                address:"0x529B1cCDB8a02571Ad22c0e534D18Dd7c66d1E01",
                privateKey:'9ba2a9dd26361c2db2025f58c6eee59810823969d857182a876c52f8cafbadca'
            },
        },
        LTC:{
            nodeURL:'http://localhost:9332', //19332 testnet 19443 regtest
            decimals:8,
            minConfirm: 4,
            listenerTimer:2.5*60*1000,
            socketURL:'tcp://127.0.0.1:3000',
            securePlatformWallet:{
                address:'',
                privateKey:''
            },
            network:{ //main
                messagePrefix: '\x19Litecoin Signed Message:\n',
                bip32: {
                  public: 0x019da462,
                  private: 0x019d9cfe
                },
                pubKeyHash: 0x30,
                scriptHash: 0x32,
                wif: 0xb0
            }
        },
        BTC:{
            listenerTimer:10*60*1000,
            nodeURL:'http://localhost:8332', //18332 testnet 18443 regtest 
            decimals:8,
            minConfirm: 2,
            socketURL:'tcp://127.0.0.1:3001',
            securePlatformWallet:{
                address:'',
                privateKey:'',
            },
            network:{ //main
                messagePrefix: '\x18Bitcoin Signed Message:\n',
                bech32: 'bc',
                bip32: {
                    public: 0x0488b21e,
                    private: 0x0488ade4
                },
                pubKeyHash: 0x00,
                scriptHash: 0x05,
                wif: 0x80
            },
            
        },
        DASH:{
            listenerTimer:2.5*60*1000,
            nodeURL:'http://localhost:9998', //19998 testnet 18332 regtest
            decimals:8,
            minConfirm: 6,
            socketURL:'tcp://127.0.0.1:3002',
            securePlatformWallet:{
                address:'',
                privateKey:''
            },
            network:{ //mainnet
                messagePrefix: '\x19Dash Signed Message:\n',
                bip32: {
                  public: 0x0488b21e,
                  private: 0x0488ade4
                },
                pubKeyHash: 0x4c,
                scriptHash: 0x10,
                wif: 0xcc
            }
        },
        ZEC:{
            listenerTimer:2.5*60*1000,
            nodeURL:'http://localhost:8232', //18232 testnet 18232 regtest
            decimals:8,
            minConfirm: 8,
            socketURL:'tcp://127.0.0.1:3003',
            securePlatformWallet:{
                address:'',
                privateKey:''
            },
            network:{ //mainnet
                messagePrefix: '\x19Zcash Signed Message:\n',
                bip32: {
                  public: 0x0488b21e, 
                  private: 0x0488ade4
                },
                pubKeyHash: 0x1cb8,
                scriptHash: 0x1cbd,
                wif: 0x80
            }
        },
        DOGE:{
            listenerTimer:1*60*1000,
            nodeURL:'http://localhost:22555', //18332 testnet 18443 regtest
            decimals:8,
            minConfirm: 6,
            socketURL:'tcp://127.0.0.1:3004',
            securePlatformWallet:{
                address:'',
                privateKey:'',
            },
            network:{ //mainnet
                messagePrefix: '\x18Dogecoin Signed Message:\n',
                bip32: {
                    public: 0x02facafd,
                    private: 0x02fac398
                },
                pubKeyHash: 0x1e,
                scriptHash: 0x16,
                wif: 0x9e
            }
            
        },
        // BCH:{
        //     listenerTimer:10*60*1000,
        //     nodeURL:'http://localhost:8332', //18332 testnet 18332 regtest
        //     decimals:8,
        //     socketURL:'tcp://127.0.0.1:3005',
        //     securePlatformWallet:{
        //         address:'',
        //         privateKey:'',
        //         legacyAddress:''
        //     },
        //     network:{ //mainnet
        //         messagePrefix: '\x18Bitcoin Signed Message:\n',
        //         bech32: 'bc',
        //         bip32: {
        //             public: 0x0488b21e,
        //             private: 0x0488ade4
        //         },
        //         pubKeyHash: 0x00,
        //         scriptHash: 0x05,
        //         wif: 0x80
        //     },
        // }
    },


};