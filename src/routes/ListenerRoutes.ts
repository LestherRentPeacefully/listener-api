import express = require("express");
import {txListener} from '../services/TransactionListeners/Altcoin/Listener';

import {CryptoService} from '../services/CryptoService/CryptoService';


const cryptoService = new CryptoService();

const ListenerRoutes = express.Router();

ListenerRoutes.post('/new-tx', ((req, res)=>{

  res.send(true);
  
  if(req.body.currency && req.body.hash){
    txListener.next({currency:req.body.currency, hash:req.body.hash});
  }


}));

ListenerRoutes.post('/getPlaformBalances', ((req, res)=>{

  cryptoService.getPlaformBalances()
    .then(info =>res.send({success: true, data: info, error: null}))
    .catch(err => res.send({success: false, data: null, error: {message: err}}));

}));


export {ListenerRoutes};