import * as admin from "firebase-admin";
import {BigNumber} from 'bignumber.js';
const fetch = require('node-fetch');
import {environment} from '../../environments/environment';
import {SharedService} from '../Shared/SharedService';

const sharedService = new SharedService();

export class CryptoPriceListener {

  watchForPrices(){
    let interval = setInterval(async ()=>{
      try {
        await this.updatePrices();
      } catch (err) {
        console.log('Error watchForPrices. Date: ', new Date());
        console.log(err.message || err);
      }
    }, 5*60*1000); // 5 min
  }


  private async updatePrices(){
    let prices = await this.getPrices();
    const db = admin.firestore();
    const batch = db.batch();
    for(let currency of sharedService.currencies){
      batch.update(db.doc(`currencies/${currency}`), {
        price: prices[currency],
        modificationDate: new Date()
      });
    }
    return batch.commit();
  }



  private async getPrices(){
    let res = await fetch(`https://min-api.cryptocompare.com/data/pricemulti?fsyms=${sharedService.currencies.join(',')}&tsyms=USD`, {
      method: 'GET'
    });

    let response = await res.json();
    return response;
  }


}