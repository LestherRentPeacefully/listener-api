import {AltcoinService} from '../TransactionListeners/Altcoin/AltcoinService';
import {EthereumService} from '../TransactionListeners/Ethereum/EthereumService';
import {environment} from '../../environments/environment';

const altcoinService = new AltcoinService();

const ethereumService = new EthereumService();


export class CryptoService {

  async getPlaformBalances() {
   const currencies = Object.keys(environment.blockchain);

   const arrayOfPromises = currencies.map(currency => this.getBalance(currency));

   return (await Promise.all(arrayOfPromises)).map((value, index) => ({amount: value, symbol: currencies[index]}));
  }


  private getBalance(currency: string): Promise<string> {
    return new Promise(async(resolve, reject)=>{
      try {
        let balance = '0';

        if(currency === 'ETH') {
          balance = ethereumService.toEther(await ethereumService.getBalance(environment.blockchain.ETH.securePlatformWallet.address));
        
        } else {
          balance = (await altcoinService.getBalance(currency, environment.blockchain[currency].minConfirm)).toString();
        }

        resolve(balance);

      } catch (err) {
        reject(err.message || err);
      }
    });
  }
}