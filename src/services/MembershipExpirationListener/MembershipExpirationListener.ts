import * as admin from "firebase-admin";
import {BigNumber} from 'bignumber.js';
import {SharedService} from '../Shared/SharedService';

const sharedService = new SharedService();



export class MembershipExpirationListener {


  async watchForExpiredMemberships(){
    this.updateExpiredMemberships().catch(err => console.log('Error:', err.message || err));

    let interval = setInterval(async ()=>{
      try {
        await this.updateExpiredMemberships();
      } catch (err) {
        console.log('Error watchForExpiredMemberships');
        console.log(err.message || err);
      }
    }, 3600*1000); // 1h
  }


  private async updateExpiredMemberships(){
    const db = admin.firestore();
    
    let [usersInfo, currencies] = (await Promise.all([
      db.collection(`users`).where('membership.nextPayment','<=',Date.now()).get(),
      db.collection('currencies').get()
    ])).map(snapshot=>snapshot.docs.map(doc=>doc.data()));

    for(let userInfo of usersInfo){

      const batch = db.batch();

      const id = sharedService.generateId();


      let currency = userInfo.membership.paymentMethod.currency;
      
      let lastPriceForPlan = currencies.find(item=>item.currency == currency);
      
      let balance = (await db.collection(`balances`).where('uid','==',userInfo.uid).where('currency','==',currency).limit(1).get()).docs[0].data();

      let amountToPay = sharedService.formatCryptoAmount(currency, new BigNumber(sharedService.pricings[userInfo.membership.pricingPlan].amount).div(lastPriceForPlan.price.USD));
      
      
      if (new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay)) { // SELECTED PAYMENT METHOD HAS ENOUGH MONEY

        const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();
        
        batch.update(db.doc(`balances/${balance.id}`), {
          available: new BigNumber(balance.available).minus(amountToPay).toString(10)
        });

        batch.update(db.doc(`totalUsersBalances/${currency}`), {
          amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
        });

        batch.update(db.doc(`users/${userInfo.uid}`), {
          'membership.nextPayment': sharedService.addMonths(new Date(), 1).getTime()
        });

        // batch.update(db.doc(`public/${userInfo.uid}`), {
        //   'membership.nextPayment': sharedService.addMonths(new Date(), 1).getTime()
        // });

        batch.set(db.doc(`transactions/${id}`), {
          currency,
          type: 'membership',
          details: `Payment for renewal for ${userInfo.membership.pricingPlan} plan (${sharedService.pricings[userInfo.membership.pricingPlan].amount})`,
          processed: true,
          status: 'confirmed',
          amount: amountToPay,
          uid: userInfo.uid,
          id,
          creationTS: Date.now()
        });

        const notifId = sharedService.generateId();
        batch.set(db.doc(`users/${userInfo.uid}/notifications/${notifId}`), {
          title: `Membership renewed`,
          content: `Your membership has been renewed`,
          id: notifId,
          creationTS: Date.now(),
        });

        batch.update(db.doc(`users/${userInfo.uid}/settings/preferences`), {
          'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
        });



      } else{ // SELECTED PAYMENT METHOD DOESN'T HAVE ENOUGH MONEY. LET'S FIND THE HIGHEST BALANCE TO PAY FOR BILLING

        let balances = (await db.collection(`balances`).where('uid','==',userInfo.uid).get()).docs.map(doc=>doc.data());

        balances.sort((balance1, balance2)=> {
          let priceInUSD1 = currencies.find(item=> item.currency == balance1.currency).price.USD;
          let priceInUSD2 = currencies.find(item=> item.currency == balance2.currency).price.USD;
          return new BigNumber(balance2.available).times(priceInUSD2).minus(new BigNumber(balance1.available).times(priceInUSD1)).toNumber();
        });

        let balance = balances[0];

        let currency = balance.currency;

        const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();

        let lastPriceForPlan = currencies.find(item=> item.currency == currency);

        let amountToPay = sharedService.formatCryptoAmount(currency, new BigNumber(sharedService.pricings[userInfo.membership.pricingPlan].amount).div(lastPriceForPlan.price.USD));
        

        if (new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay)) { // HIGHEST BALANCE HAS ENOUGH MONEY

          batch.update(db.doc(`balances/${balance.id}`), {
            available: new BigNumber(balance.available).minus(amountToPay).toString(10)
          });
          batch.update(db.doc(`totalUsersBalances/${currency}`), {
            amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
          });
  
          batch.update(db.doc(`users/${userInfo.uid}`), {
            'membership.nextPayment': sharedService.addMonths(new Date(), 1).getTime()
          });
  
          // batch.update(db.doc(`public/${userInfo.uid}`), {
          //   'membership.nextPayment': sharedService.addMonths(new Date(), 1).getTime()
          // });

          batch.set(db.doc(`transactions/${id}`), {
            currency,
            type: 'membership',
            details: `Payment for renewal for ${userInfo.membership.pricingPlan} plan (${sharedService.pricings[userInfo.membership.pricingPlan].amount})`,
            processed: true,
            status: 'confirmed',
            amount: amountToPay,
            uid: userInfo.uid,
            id,
            creationTS: Date.now()
          });

          const notifId = sharedService.generateId();
          batch.set(db.doc(`users/${userInfo.uid}/notifications/${notifId}`), {
            title: `Membership renewed`,
            content: `Your membership has been renewed`,
            id: notifId,
            creationTS: Date.now(),
          });

          batch.update(db.doc(`users/${userInfo.uid}/settings/preferences`), {
            'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
          });



        } else { // HIGHEST BALANCE DOESN'T HAVE ENOUGH MONEY. DOWNGRADING PRICING PLAN TO FREE
          batch.update(db.doc(`users/${userInfo.uid}`), {
            'membership.pricingPlan':'free',
            'membership.nextPayment':admin.firestore.FieldValue.delete(),
            'membership.paymentMethod':admin.firestore.FieldValue.delete()
          });
      
          // batch.update(db.doc(`public/${userInfo.uid}`), {
          //   'membership.pricingPlan':'free',
          //   'membership.nextPayment':admin.firestore.FieldValue.delete(),
          //   'membership.paymentMethod':admin.firestore.FieldValue.delete()
          // });

          const notifId = sharedService.generateId();
          batch.set(db.doc(`users/${userInfo.uid}/notifications/${notifId}`), {
            title: `Membership downgraded`,
            content: `You don't have sufficient funds to renew your membership`,
            id: notifId,
            creationTS: Date.now(),
          });

          batch.update(db.doc(`users/${userInfo.uid}/settings/preferences`), {
            'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
          });

        }

      }
      
      await batch.commit();


    }
  }






}