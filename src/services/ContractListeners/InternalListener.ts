import * as admin from "firebase-admin";
import * as web3 from 'web3';
import {environment} from '../../environments/environment';
import { SharedService } from '../Shared/SharedService';
import { Observable } from 'rxjs';
import {take, tap} from 'rxjs/operators';
// import {BigNumber} from 'bignumber.js';

const sharedService = new SharedService();

export class InternalListener{
	private web3:any;
	private txs:any = {};
	
	constructor(){
		this.web3 = new web3();
		this.web3.setProvider(new web3.providers.HttpProvider(environment.blockchain.ETH.nodeURL));
	}

	public watchForAllTxs():void{

		this.watchForTxs('contractCreation');

		this.watchForTxs('saveDocLocation');

		// this.watchForTxs('approveAgreement');

		// this.watchForTxs('modifyRent');

		// this.watchForTxs('openTicket');

		// this.watchForTxs('closeTicket');

		// this.watchForTxs('startDispute');

		// this.watchForTxs('endDispute');

		// this.watchForTxs('deposit');

		// this.watchForTxs('withdraw');
	}

	private watchForTxs(action: string):void{

		this.watchForPendingTxs(action).pipe(take(1), tap(()=>{
			
			this.watchForTxsConfirmations(action);

		})).subscribe();

	}

	private watchForPendingTxs(action:string):Observable<boolean>{
		return new Observable(observer=>{

			const db = admin.firestore();
			db.collection(`transactions`)
			.where('type','==','contractExecution')
			.where('action','==',action)
			.onSnapshot((querySnapshot)=>{
				
				this.txs[action] = [];
				
				querySnapshot.forEach(doc=> this.txs[action].push(doc.data()) );
				observer.next(true);
				
			});
		});
	}

	private watchForTxsConfirmations(action:string){
		const db = admin.firestore();

		let interval = setInterval(async ()=>{

			
			if(this.txs[action]){
				
				let currentBlock = this.web3.eth.blockNumber;
				

				for(let tx of this.txs[action]){

					const batch = db.batch();
					let atLeastOneBatch = false;
					
					let transaction = this.web3.eth.getTransaction(tx.txHash);

					if (transaction) { //If the tx exists 

						if (transaction.blockNumber) { //If it's confirmed

							// if(tx.status =='pending'){ //It was marked as pending before, then it went from pending to confirmed

								// atLeastOneBatch = true;
								// batch.update(db.doc(`transactions/${tx.id}`), {status:'confirmed'});
							// }

							if(currentBlock-transaction.blockNumber>=12){ //If it has more than 12 confirmations

								let transactionReceipt = this.web3.eth.getTransactionReceipt(tx.txHash);

								atLeastOneBatch = true;
								batch.delete(db.doc(`transactions/${tx.id}`)); //Delete the tx from database

								if (transactionReceipt.status=='0x1') { //If it succeeded


									if(action =='contractCreation'){ //update status to confirmed

										const notifId = sharedService.generateId();


										if(tx.myProperty) {
											batch.update(db.doc(`myProperties/${tx.myProperty.id}`), {
												'smartContract.status': 'confirmed',
												'smartContract.address': transactionReceipt.contractAddress.toLowerCase(),
												'smartContract.gasUsed': transactionReceipt.gasUsed
											});

										} else if(tx.agreement) {
											batch.update(db.doc(`agreements/${tx.agreement.id}`), {
												'smartContract.status': 'confirmed',
												'smartContract.address': transactionReceipt.contractAddress.toLowerCase(),
												'smartContract.gasUsed': transactionReceipt.gasUsed
											});
										}

										batch.set(db.doc(`users/${tx.uid}/notifications/${notifId}`), {
											title: `Contract created`,
											content: `Your contract has been successfully created. You may now upload your signed documents to the blockchain`,
											id: notifId,
											creationTS: Date.now(),
										});
						
										batch.update(db.doc(`users/${tx.uid}/settings/preferences`), {
											'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
										});

										
							
									} else if(action === 'saveDocLocation') {
										const notifId1 = sharedService.generateId();
										const notifI2 = sharedService.generateId();

										if(tx.myProperty) {
											batch.update(db.doc(`myProperties/${tx.myProperty.id}/tenants/${tx.myProperty.tenant}/signaturesRequest/${tx.myProperty.signatureRequestSelectedID}`), {
												'smartContract.status': 'confirmed',
												'smartContract.gasUsed': transactionReceipt.gasUsed
											});

											batch.set(db.doc(`users/${tx.myProperty.tenant}/notifications/${notifI2}`), {
												title: `Document saved on blockchain`,
												content: `Your document has been successfully saved on the blockchain. No one can alter it now`,
												id: notifI2,
												creationTS: Date.now(),
											});
							
											batch.update(db.doc(`users/${tx.myProperty.tenant}/settings/preferences`), {
												'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
											});

										} else if(tx.agreement) {
											batch.update(db.doc(`agreements/${tx.agreement.id}/signaturesRequest/${tx.agreement.signatureRequestSelectedID}`), {
												'smartContract.status': 'confirmed',
												'smartContract.gasUsed': transactionReceipt.gasUsed
											});

											batch.set(db.doc(`users/${tx.agreement.landlord}/notifications/${notifI2}`), {
												title: `Document saved on blockchain`,
												content: `Your document has been successfully saved on the blockchain. No one can alter it now`,
												id: notifI2,
												creationTS: Date.now(),
											});
							
											batch.update(db.doc(`users/${tx.agreement.landlord}/settings/preferences`), {
												'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
											});

										}

										batch.set(db.doc(`users/${tx.uid}/notifications/${notifId1}`), {
											title: `Document saved on blockchain`,
											content: `Your document has been successfully saved on the blockchain. No one can alter it now`,
											id: notifId1,
											creationTS: Date.now(),
										});
						
										batch.update(db.doc(`users/${tx.uid}/settings/preferences`), {
											'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
										});

										
									}
									


								} else { //If it failed


									if(action === 'contractCreation'){ //Delete failed agreement
										const notifId = sharedService.generateId();

										if(tx.myProperty) {
											batch.update(db.doc(`myProperties/${tx.myProperty.id}`), {
												smartContract: admin.firestore.FieldValue.delete()
											});
											
										} else if(tx.agreement) {
											batch.update(db.doc(`agreements/${tx.agreement.id}`), {
												smartContract: admin.firestore.FieldValue.delete()
											});
										}

										batch.set(db.doc(`users/${tx.uid}/notifications/${notifId}`), {
											title: `Contract creation transaction failed`,
											content: `You can get more info on a blockchain explorer with the following hash: ${tx.txHash}`,
											id: notifId,
											creationTS: Date.now(),
										});
						
										batch.update(db.doc(`users/${tx.uid}/settings/preferences`), {
											'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
										});
										// batch.delete(db.doc(`rentalAgreements/${tx.id}`));

										
									} else if(action === 'saveDocLocation') {
										const notifId = sharedService.generateId();

										if(tx.myProperty) {
											batch.update(db.doc(`myProperties/${tx.myProperty.id}/tenants/${tx.myProperty.tenant}/signaturesRequest/${tx.myProperty.signatureRequestSelectedID}`), {
												smartContract: admin.firestore.FieldValue.delete()
											});
										
										} else if(tx.agreement) {
											batch.update(db.doc(`agreements/${tx.agreement.id}/signaturesRequest/${tx.agreement.signatureRequestSelectedID}`), {
												smartContract: admin.firestore.FieldValue.delete()
											});
										}

										batch.set(db.doc(`users/${tx.uid}/notifications/${notifId}`), {
											title: `Document save failed`,
											content: `You can get more info on a blockchain explorer with the following hash: ${tx.txHash}`,
											id: notifId,
											creationTS: Date.now(),
										});
						
										batch.update(db.doc(`users/${tx.uid}/settings/preferences`), {
											'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
										});
									}
								
								}

							}

						} else { //If it's pending
						}
						
					} else{ //If it doesn't exist. (might have dissappeared)

						atLeastOneBatch = true;
						batch.delete(db.doc(`transactions/${tx.id}`)); //Delete the tx from database

						if(action =='contractCreation'){
							if(tx.myProperty) {
								batch.update(db.doc(`myProperties/${tx.myProperty.id}`), {
									smartContract: admin.firestore.FieldValue.delete()
								});
								
							} else if(tx.agreement) {
								batch.update(db.doc(`agreements/${tx.agreement.id}`), {
									smartContract: admin.firestore.FieldValue.delete()
								});
							}

						} else if(action === 'saveDocLocation') {
							if(tx.myProperty) {
								batch.update(db.doc(`myProperties/${tx.myProperty.id}/tenants/${tx.myProperty.tenant}/signaturesRequest/${tx.myProperty.signatureRequestSelectedID}`), {
									smartContract: admin.firestore.FieldValue.delete()
								});
							
							} else if(tx.agreement) {
								batch.update(db.doc(`agreements/${tx.agreement.id}/signaturesRequest/${tx.agreement.signatureRequestSelectedID}`), {
									smartContract: admin.firestore.FieldValue.delete()
								});
							}
						}
					}


					if (atLeastOneBatch) batch.commit().catch(err=>'');

				}

				
			}


		}, environment.listenerTimer);

	}




}