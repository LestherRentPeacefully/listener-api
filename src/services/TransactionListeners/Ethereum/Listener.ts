import * as admin from "firebase-admin";
import * as web3 from 'web3';
import {environment} from '../../../environments/environment';
import {BigNumber} from 'bignumber.js';
import { Observable, combineLatest } from 'rxjs';
import {take, tap, filter} from 'rxjs/operators';
import {EthereumService} from './EthereumService';
import {SharedService} from '../../Shared/SharedService';
import { timer } from 'rxjs';

const sharedService = new SharedService();

const ethereumService = new EthereumService();

interface depositWallet {address:string, privateKey:string, searchable:string, uid:string, id:string, currency:string};


export class Listener{
  private web3:any;
  private filter: any;
  private filterId: string;
  private txs:any = {};
  private wallets:depositWallet[];
  private securePlatformWallet:{address:string, privateKey:string};

  constructor(){
    this.securePlatformWallet = environment.blockchain.ETH.securePlatformWallet;
		this.web3 = new web3();
    this.web3.setProvider(new web3.providers.HttpProvider(environment.blockchain.ETH.nodeURL));
  }


  public watchForDeposits(){

    combineLatest( this.getDepositsWallets().pipe(take(1)), this.getPendingTxs('deposit').pipe(take(1)) )
    .pipe(filter(([a, b])=> a && b), tap(()=>{

      this.watchForPendingTxs();
      this.watchForTxsConfirmations('deposit');

    })).subscribe();
  }
  

  private getDepositsWallets():Observable<boolean>{
    return new Observable(observer=>{
      const db = admin.firestore();
      db.collection(`wallets/deposit/private`)
      .where('currency','==','ETH')
      .onSnapshot(querySnapshot=>{
        
        this.wallets = [];
        
        querySnapshot.forEach(doc=>	this.wallets.push(<depositWallet>doc.data()));
        observer.next(true);
      });
    });
  }
  


  private getPendingTxs(type:string):Observable<boolean>{
    return new Observable(observer=>{
      const db = admin.firestore();
      db.collection(`transactions`)
      .where('type','==',type)
      .where('currency','==','ETH')
      .where('status','==','pending')
      .get()
      .then((querySnapshot)=>{
        
        this.txs[type] = [];
        
        querySnapshot.forEach(doc=> this.txs[type].push(doc.data()) );
        observer.next(true);        
      });
    });
  }



  private watchForPendingTxs() {
    this.filter = this.web3.eth.filter('latest');
    this.filter.watch(async (err: any, blockHash: string) => {
      if (!err){
        const block = await ethereumService.getBlock(blockHash);
        for(const transaction of block.transactions) {
          if(transaction && transaction.to){
            const to = transaction.to.toLowerCase();
            const receiverWallet = this.wallets.find(wallet=>wallet.searchable==to);
            if(receiverWallet){ //ETH deposited by user
              // console.log('ETH deposited by user. pending tx');
              this.uploadPendingTx(receiverWallet, transaction).catch(err=>console.log(err.message || err));
            }
          }
        }

      } else {
         console.log('Error watchForPendingTxs. Date: ', new Date());
        console.log(err.message || err);
        this.filter.stopWatching();
        this.filter = null;
        this.watchForPendingTxs();       
      }
    });
  }


    // this.filter = this.web3.eth.filter('pending');

    // this.filter.watch(async (err: any, hash: string) => {
    //   if (!err){
    //     let transaction = await ethereumService.getTransaction(hash);
    //     // console.log('hash:', transaction.hash);
  
    //     if(transaction && transaction.to){
    //       let to = transaction.to.toLowerCase();
    //       let receiverWallet = this.wallets.find(wallet=>wallet.searchable==to);
  
    //       if(receiverWallet){ //ETH deposited by user
    //         console.log('Pending transaction received. Uploading');
    //         // console.log('ETH deposited by user. pending tx');
    //         this.uploadPendingTx(receiverWallet, transaction).catch(err=>console.log(err.message || err));
    //       }
    //     }

    //   }else{
    //     console.log('Error watchForPendingTxs. Date: ', new Date());
    //     console.log(err.message || err);
    //     this.filter.stopWatching();
    //     this.filter = null;
    //     this.watchForPendingTxs();
    //     // throw new Error(err.message || err);
    //   }

    // });

  // }



  // private createPendingTxFilter(): Promise<string> {
  //   return ethereumService.handleRPCRequest('eth_newPendingTransactionFilter');
  // }

  // private watchPendingTxFilter(filterId: string):Promise<string[]> {
  //   return ethereumService.handleRPCRequest('eth_getFilterChanges', filterId);
  // }

  // private uninstallPendingTxFilter(filterId: string) {
  //   return ethereumService.handleRPCRequest('eth_uninstallFilter', filterId);
  // }

  
  private async uploadPendingTx(wallet:depositWallet, transaction:any){
    const db = admin.firestore();

    if(!((this.txs['deposit'] as Array<any>).find(tx=> tx.currency=='ETH' && tx.searchable==transaction.hash.toLowerCase())) ){ //The pending transaction is not on DB yet
    
      const id = db.collection('uniqueID').doc().id;

      let txObj = {
        type:'deposit',
        currency:'ETH',
        status:'pending',
        uid:wallet.uid,
        txHash:transaction.hash,
        searchable:transaction.hash.toLowerCase(),
        id:id
      }

      await db.doc(`transactions/${id}`).set(txObj);

      this.txs['deposit'].push(txObj);

      return true;

    }else{ //The pending transaction is already on DB
      return true;
    }
  }




  private watchForTxsConfirmations(type:string){
    const db = admin.firestore();

    let interval = setInterval(async ()=>{

      if(this.txs[type]){
        
        let currentBlock = await ethereumService.getBlockNumber();

        for(let tx of this.txs[type]){
          
          let transaction = await ethereumService.getTransaction(tx.txHash);

          if (transaction) { //If the tx exists 

            if(transaction.blockNumber && currentBlock-transaction.blockNumber>=environment.blockchain.ETH.minConfirm){ //If it has more than 12 confirmations

              let transactionReceipt = await ethereumService.getTransactionReceipt(tx.txHash);

              if (transactionReceipt.status=='0x1') { //If it succeed
                
                let to = transaction.to.toLowerCase();
                let receiverWallet = this.wallets.find(wallet=>wallet.searchable==to);

                if(receiverWallet){ //ETH deposited by user
                  // console.log('ETH deposited by user. Confirmed tx');
                  this.forwardMoney(receiverWallet, tx).catch(err=>console.log(err.message || err));
      
                }else if(to==this.securePlatformWallet.address.toLowerCase()){ //ETH received by platform
                  // console.log('ETH received in platform wallet. Confirmed tx');
                  await this.updateBalances(transaction, tx, transactionReceipt).catch(err=>console.log(err.message || err));
                }
                
              }else{ //If it failed
                db.doc(`transactions/${tx.id}`).delete().then(()=> this.deleteTx(type, tx.id) ).catch(err=>console.log(err.message || err)); //Delete the tx from database
                
              }

            }


          }else{ //If it doesn't exist. (might have dissappeared)
            db.doc(`transactions/${tx.id}`).delete().then(()=> this.deleteTx(type, tx.id) ).catch(err=>console.log(err.message || err)); //Delete the tx from database
          }
        }

        
      }
 
    }, environment.blockchain.ETH.listenerTimer);
  }



  private async forwardMoney(wallet:depositWallet, tx:any){
    
    let gasPrice = await ethereumService.gasPrice();
    let balance = await ethereumService.getBalance(wallet.address);
    let gasLimit = '21000';

    let fee = new BigNumber(gasPrice).times(gasLimit).toString(10);

    let amountToSend = new BigNumber(balance).minus(fee).toString(10);
    
    if(new BigNumber(amountToSend).isGreaterThan(0)){ //There's enough ETH to forward
      // console.log('Forwarding ETH to the platform wallet...');
      let hash = await ethereumService.transfer(wallet.address, wallet.privateKey, this.securePlatformWallet.address, amountToSend, gasPrice, gasLimit);
      // console.log('hash:', hash);
      const db = admin.firestore();
      const batch = db.batch();
      const id = db.collection('uniqueID').doc().id;

      let txObj = {
        type: 'deposit',
        currency: 'ETH',
        status: 'pending',
        uid: wallet.uid,
        processed: true,
        amount: ethereumService.toEther(amountToSend),
        fee: ethereumService.toEther(fee),
        txHash: hash,
        searchable: hash.toLowerCase(),
        id: id,
        creationTS: new Date().getTime()
      }

      batch.set(db.doc(`transactions/${id}`), txObj);
      batch.delete(db.doc(`transactions/${tx.id}`));

      await batch.commit();

      (<any[]>this.txs['deposit']).push(txObj);

      this.deleteTx('deposit', tx.id);

      return true;


    }else{
      // console.log('Amount too small to forward');
      return true;
    }
  }



  private async updateBalances(transaction:any, tx:any, transactionReceipt:any){
    const db = admin.firestore();
    const batch = db.batch();

    let [balancesSnapshot, txSnapshot, userSnapshot] =  await Promise.all([
      db.collection(`balances`)
        .where('currency','==','ETH')
        .where('uid','==',tx.uid)
        .get(), 
      db.collection('transactions')
        .where('currency','==','ETH')
        .where('status','==','confirmed')
        .where('searchable','==',transaction.hash.toLowerCase())
        .limit(1)
        .get(),
      db.doc(`users/${tx.uid}`).get()
    ]);

    
    if(txSnapshot.empty){ //The confirmed transaction is not on DB yet
      const userInfo = userSnapshot.data();
      
      const totalUsersBalance = (await db.doc(`totalUsersBalances/ETH`).get()).data();
      // console.log('Updating balance');
      let balances = balancesSnapshot.docs[0].data();
      
      batch.update(db.doc(`transactions/${tx.id}`), {
        status: 'confirmed',
        amount: ethereumService.toEther(transaction.value),
        fee: ethereumService.toEther(new BigNumber(transaction.gasPrice).times(transactionReceipt.gasUsed)),
        creationTS: new Date().getTime()
      });
      batch.update(db.doc(`balances/${balances.id}`), {
        available:new BigNumber(balances.available).plus(ethereumService.toEther(transaction.value)).toString(10) 
      });
      batch.update(db.doc(`totalUsersBalances/ETH`), {
        amount: new BigNumber(totalUsersBalance.amount).plus(ethereumService.toEther(transaction.value)).toString()
      });

      const notifId = sharedService.generateId();
      batch.set(db.doc(`users/${userInfo.uid}/notifications/${notifId}`), {
        title: `Deposit received`,
        content: `You've received ${ethereumService.toEther(transaction.value)} ETH in your wallet`,
        id: notifId,
        creationTS: Date.now(),
      });

      batch.update(db.doc(`users/${userInfo.uid}/settings/preferences`), {
        'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
      });
      
      await batch.commit();

      this.deleteTx('deposit', tx.id);

      return true;

    }else{ //The confirmed transaction is already on DB
      this.deleteTx('deposit', tx.id);
      return true;
    }
  }



  private deleteTx(type:string, id:string){
    (<any[]>this.txs[type]).splice((<any[]>this.txs[type]).findIndex(_tx=> _tx.id==id), 1);
  }


  

  


  
  

}