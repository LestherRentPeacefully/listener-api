import {environment} from '../../../environments/environment';
import * as web3 from 'web3';
import * as Tx from 'ethereumjs-tx';
import {BigNumber} from 'bignumber.js';
const fetch = require('node-fetch');


export class EthereumService{
  private web3:web3;
	// private myContractInstance:any;
	constructor(){
		this.web3 = new web3();
		this.web3.setProvider(new web3.providers.HttpProvider(environment.blockchain.ETH.nodeURL));

		if (this.web3.isConnected()) {
		} else {
      let err = new Error("A web3 valid instance must be provided");
      err.name = "Web3InstanceError";
      throw err;
		}		
  }
  
  public async getTransaction(txHash:string){
		return this.web3.eth.getTransaction(txHash);
	}

	public async getTransactionReceipt(txHash:string){
		return this.web3.eth.getTransactionReceipt(txHash);
  }

  public async getBlockNumber(){
    return this.web3.eth.blockNumber;
  }

  public async getBlock(blockHashOrBlockNumber: string | number) {
    return this.web3.eth.getBlock(blockHashOrBlockNumber, true);
  }

  public async getBalance(address: string){
    return this.web3.eth.getBalance(address).toString(10);
  }

  public async gasPrice(){
    return this.web3.eth.gasPrice.toString(10);
  }
  
  public transfer(from:string, privateKey:string, to:string, amount:string | number, _gasPrice?, gasLimit?):Promise<string>{
		return new Promise((resolve,reject)=>{
			try{
					let gasPrice = _gasPrice || this.web3.eth.gasPrice;
					let estimateGas = gasLimit || this.web3.eth.estimateGas({
            from:from,
            to: to,
            value: this.web3.toHex(amount)
          });

			    let rawTx:any = {
		        nonce: this.web3.toHex(this.web3.eth.getTransactionCount(from)),
		        gasPrice: this.web3.toHex(gasPrice),
            gasLimit: this.web3.toHex(estimateGas),
            to:to,
            value:this.web3.toHex(amount)
			    };

	        // Generate tx
	        let tx = new Tx(rawTx);
	        tx.sign(Buffer.from(privateKey, 'hex')); //Sign transaction
	        let serializedTx = `0x${tx.serialize().toString('hex')}`;
          this.web3.eth.sendRawTransaction(serializedTx, (err, hash)=> {
            if(err){
              reject(err.message || err);
            } else{
              resolve(hash);
            }               
          });


			}catch(err){
				reject(err.message);
			}
		});
  }


  public toEther(amount:number | string | BigNumber){
    return new BigNumber(amount).times(new BigNumber(10).pow(-18)).decimalPlaces(18).toString(10);
  }

  public toWei(amount:number | string | BigNumber){
    return new BigNumber(amount).decimalPlaces(18).times(new BigNumber(10).pow(18)).toString(10);
  }


  public handleRPCRequest(method:string, ...params:(string | number | boolean | string[])[]): Promise<any> {
    return new Promise(async (resolve, reject)=>{
      try {
        const res = await fetch(environment.blockchain.ETH.nodeURL, {
          method: 'POST',
          body:  JSON.stringify({
            "id": "curlRPC",
            jsonrpc: "2.0",
            method,
            params
          }),
          headers: {
            'Content-Type': 'application/json'
          },

        });

        const response = await res.json();

        if (response.error) {
          reject(response.error.message); return;
        }

        resolve(response.result);


      } catch (err) {
        reject(err.message || err);
      }
    });
  }
}