import * as admin from "firebase-admin";
import {environment} from '../../../environments/environment';
import {BigNumber} from 'bignumber.js';
import {AltcoinService} from './AltcoinService';
import { BehaviorSubject } from 'rxjs';
import {tap, filter} from 'rxjs/operators';
import {SharedService} from '../../Shared/SharedService';

const sharedService = new SharedService();

const altcoinService = new AltcoinService();

interface depositWallet {address:string, privateKey:string, searchable:string, uid:string, id:string, currency:string};

export const txListener = new BehaviorSubject({currency:null, hash:null});


export class Listener{
  private txs:any = {};

  public currency:string;
  
  constructor(currency:string){
    this.currency = currency;
  }



  public async watchForDeposits(){
    await this.getPendingTxs(this.currency, 'deposit');
    // console.log('Wallets and txs downloaded!');
    this.watchForPendingTxs(this.currency);
    
    this.watchForTxsConfirmations(this.currency, 'deposit');
  }



  private async getPendingTxs(currency:string, type:string){
    const db = admin.firestore();

    let querySnapshot = await db.collection(`transactions`)
      .where('type','==',type)
      .where('currency','==',currency)
      .where('status','==','pending')
      .get();
      
    this.txs[type] = [];
    
    querySnapshot.forEach(doc=> this.txs[type].push(doc.data()) );
  }



  private watchForPendingTxs(currency:string){

    txListener.pipe(filter(tx=>tx.currency==currency && tx.hash), tap(async (tx)=>{

      const hash = tx.hash;

      let transaction = await altcoinService.getTransaction(currency, hash);
      
      if(transaction && transaction.vout){
        
        for(let vout of transaction.vout){

          if(vout.scriptPubKey && vout.scriptPubKey.addresses && vout.scriptPubKey.addresses[0]){
            let to:string = vout.scriptPubKey.addresses[0].toLowerCase();
            if(currency=='BCH') to = to.substring(to.indexOf(':') + 1);

            // let receiverWallet = this.wallets.find(wallet=>to==wallet.searchable);
            let receiverWallet = await this.getDepositWallet(currency, to);
            
            if(receiverWallet){
              // console.log(`${currency} deposited by user. Pending tx`);
              this.uploadPendingTx(currency, receiverWallet, transaction, vout).catch(err=>console.log(err.message || err));
            }
          }
        }
      }
    })).subscribe();

    
  }



  private async getDepositWallet(currency:string, address:string){
    const db = admin.firestore();
    let snapshot = await db.collection(`wallets/deposit/private`)
      .where('currency','==',currency)
      .where('searchable','==',address)
      .limit(1)
      .get();

    if(snapshot.empty) return null;
    else return <depositWallet>snapshot.docs[0].data();
  }



  private async uploadPendingTx(currency:string, wallet:depositWallet, transaction:any, vout:{value:number, scriptPubKey:{addresses:string[]}}){
    const db = admin.firestore();

    if(!((this.txs['deposit'] as Array<any>).find(tx=> tx.currency==currency && tx.searchable==transaction.txid.toLowerCase())) ){ //The pending transaction is not on DB yet
    
      const id = db.collection('uniqueID').doc().id;

      let txObj = {
        type: 'deposit',
        currency,
        status: 'pending',
        uid: wallet.uid,
        processed: true,
        amount: vout.value.toString(),
        fee: '0',
        txHash: transaction.txid,
        searchable: transaction.txid.toLowerCase(),
        id,
        creationTS: Date.now()
      }

      await db.doc(`transactions/${id}`).set(txObj);

      this.txs['deposit'].push(txObj);

      return true;

    }else{ //The pending transaction is already on DB
      return true;
    }
  }



  private watchForTxsConfirmations(currency:string, type:string){
    const db = admin.firestore();

    let interval = setInterval(async ()=>{

      if(this.txs[type]){
      
        for(let tx of this.txs[type]){

          let transaction = await altcoinService.getTransaction(currency, tx.txHash);

          if (transaction) { //If the tx exists 

            if(transaction.confirmations >= environment.blockchain[currency].minConfirm){

              for(let vout of transaction.vout){

                let to:string = vout.scriptPubKey.addresses[0].toLowerCase();
                if(currency=='BCH') to = to.substring(to.indexOf(':') + 1);

                // let receiverWallet = this.wallets.find(wallet=>to==wallet.searchable);
                let receiverWallet = await this.getDepositWallet(currency, to);

                if(receiverWallet){
                  // console.log(`${currency} deposited by user. Confirmed tx`);
                  await this.updateBalances(currency, transaction, tx, vout).catch(err=>console.log(err.message || err));
                }
              }
            }

          } else{ //If it doesn't exist. (might have dissappeared)
            db.doc(`transactions/${tx.id}`).delete().then(()=> this.deleteTx(type, tx.id) ).catch(err=>console.log(err.message || err)); //Delete the tx from database
          }
        }
      }

    }, environment.blockchain[currency].listenerTimer);
  }


  private async updateBalances(currency:string, transaction:any, tx:any, vout:{value:number, scriptPubKey:{addresses:string[]}}){
    const db = admin.firestore();
    const batch = db.batch();

    let [balancesSnapshot, txSnapshot, userSnapshot] =  await Promise.all([
      db.collection(`balances`)
        .where('currency','==',currency)
        .where('uid','==',tx.uid)
        .limit(1)
        .get(), 
      db.collection('transactions')
        .where('currency','==',currency)
        .where('status','==','confirmed')
        .where('searchable','==',transaction.txid.toLowerCase())
        .limit(1)
        .get(),
      db.doc(`users/${tx.uid}`).get()
    ]);

    
    if(txSnapshot.empty){ //The confirmed transaction is not on DB yet
      const userInfo = userSnapshot.data();
  
      const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();
      // console.log('Updating balance');
      let balances = balancesSnapshot.docs[0].data();
      
      batch.update(db.doc(`transactions/${tx.id}`), {
        status:'confirmed',
        amount: vout.value.toString(),
        creationTS: Date.now()
      });
      batch.update(db.doc(`balances/${balances.id}`), {
        available:new BigNumber(balances.available).plus(vout.value.toString()).toString(10) 
      });
      batch.update(db.doc(`totalUsersBalances/${currency}`), {
        amount: new BigNumber(totalUsersBalance.amount).plus(vout.value.toString()).toString()
      });

      const notifId = sharedService.generateId();
      batch.set(db.doc(`users/${userInfo.uid}/notifications/${notifId}`), {
        title: `Deposit received`,
        content: `You've received ${vout.value.toString()} ${currency} in your wallet`,
        id: notifId,
        creationTS: Date.now(),
      });

      batch.update(db.doc(`users/${userInfo.uid}/settings/preferences`), {
        'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
      });
      
      await batch.commit();

      this.deleteTx('deposit', tx.id);

      return true;

    }else{ //The confirmed transaction is already on DB
      this.deleteTx('deposit', tx.id);
      return true;
    }
  }



  private deleteTx(type:string, id:string){
    (<any[]>this.txs[type]).splice((<any[]>this.txs[type]).findIndex(_tx=> _tx.id==id), 1);
  }

}