// const bitcoin = require('bitcoinjs-lib');
// const zcash = require('zcashjs-lib');
import {environment} from '../../../environments/environment';
const fetch = require('node-fetch');
import {BigNumber} from 'bignumber.js';
// const coinSelect = require('coinselect');

export class AltcoinService {



  public async estimatefeePerKb(currency:string){
    if(currency=='BCH') return {feerate: await this.handleRequest(currency, 'estimatefee', 2)};
    if(currency=='PPC') return {feerate: 0.01};
    return this.handleRequest(currency, 'estimatesmartfee', 2);
  }

  public listUnspentUTXO(currency:string, address:string){
    return this.handleRequest(currency, 'listunspent', 1, 9999999, [address]);
  }

  public signrawtransaction(currency:string,rawTx:string, wif:string){
    return this.handleRequest(currency, 'signrawtransaction', rawTx, null, [wif]);
  }

  public sendrawtransaction(currency:string, rawTx:string){
    return this.handleRequest(currency, 'sendrawtransaction', rawTx);
  }

  public getBlockNumber(currency:string){
    return this.handleRequest(currency, 'getblockcount');
  }

  public async getBlock(currency:string, hashOrHeight:string | number){
    if (typeof hashOrHeight=='string') return this.getBlockByHash(currency, hashOrHeight); 
    return this.getBlockByHash(currency, (await this.getBlockHash(currency, hashOrHeight)) );
  }

  private getBlockByHash(currency:string, hash:string){
    return this.handleRequest(currency, 'getblock', hash);
  }

  private getBlockHash(currency:string, height:number){
    return this.handleRequest(currency, 'getblockhash', height);
  }

  public getTransaction(currency:string, hash:string){
    const verbose = (currency=='ZEC' || currency=='PPC')? 1 : true;
    return this.handleRequest(currency, 'getrawtransaction', hash, verbose);
  }

  public getBalance(currency: string, minConfirm: number) {
    return this.handleRequest(currency, 'getbalance', '*', minConfirm);
  }

  
  private handleRequest(currency:string, method:string, ...params:(string | number | boolean | string[])[]):Promise<any>{
    return new Promise(async (resolve, reject)=>{
      try {
        let res = await fetch(environment.blockchain[currency].nodeURL, { 
          method: 'POST',
          body:    JSON.stringify({
            id:"curltest", 
            method: method, 
            params: params
          }),
          headers: { 
            'Content-Type': 'application/json',
            'Authorization': `Basic ${Buffer.from(`${environment.altcoinNodesAuth.user}:${environment.altcoinNodesAuth.password}`).toString('base64')}`
          },
        });
  
        let response = await res.json();
        
        if (!response.error) {
          resolve(response.result);
          
        } else {
          if (method=='getrawtransaction') resolve(null);
          else reject(response.error.message);
        }
        
      } catch (err) {
        if(method=='getrawtransaction') resolve(null);
        else reject(err.message || err);
      }


    });
  }



}