import * as admin from "firebase-admin";
import {environment} from "../environments/environment";


export class Database {
	private config:any;

	constructor(){
		this.config = { 
		  credential: admin.credential.cert(environment.firebaseConfig.credential),
		  databaseURL: environment.firebaseConfig.databaseURL
		}
	}

	public init(): void {
		admin.initializeApp(this.config);
	}

}