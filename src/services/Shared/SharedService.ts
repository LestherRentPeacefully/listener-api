import {BigNumber} from 'bignumber.js';
import {environment} from '../../environments/environment';
import * as admin from "firebase-admin";

export class SharedService{

  currencies = Object.keys(environment.blockchain);


  formatCryptoAmount(currency:string, amount:string | number | BigNumber){
    return new BigNumber(amount).decimalPlaces(environment.blockchain[currency].decimals).toString(10);
  }

  capitalize(value:string){
		return value.charAt(0).toUpperCase() + value.slice(1);
  }

  addMonths(oldDate:number | Date, count:number) {
    const date = new Date(oldDate);
    const day = date.getDate();
    date.setMonth(date.getMonth() + count, 1);
    const month = date.getMonth();
    date.setDate(day);
    if (date.getMonth() !== month) date.setDate(0);
    return date;
  }

  generateId(){
    return admin.firestore().collection('uniqueID').doc().id;
  }


  public pricings = {
    free: {
      amount: 0,
      tenant: {},
      realEstateAgent: {
        maxListed: 2,
      },
      landlord: {
        maxListed: 2,
      },
    },
    basic: {
      amount: 4.95,
      tenant: {},
      realEstateAgent: {
        maxListed: 7,
      },
      landlord: {
        maxListed: 7,
      },
    },
    plus: {
      amount: 9.95,
      tenant: {},
      realEstateAgent: {
        maxListed: 15,
      },
      landlord: {
        maxListed: 15,
      },
    },
    professional: {
      amount: 14.95,
      tenant: {},
      realEstateAgent: {
        maxListed: 20,
      },
      landlord: {
        maxListed: 20,
      },
    },
    enterprise: {
      amount: 19.95,
    }
  }

}