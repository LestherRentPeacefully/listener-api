import bodyParser = require('body-parser');
import cors = require('cors');
import * as express from 'express';
import {InternalListener} from './services/ContractListeners/InternalListener';
import {Listener as ETHListener} from './services/TransactionListeners/Ethereum/Listener';
import {Listener as AltcoinListener} from './services/TransactionListeners/Altcoin/Listener';
import {MembershipExpirationListener} from './services/MembershipExpirationListener/MembershipExpirationListener';
import {CryptoPriceListener} from './services/CryptoPriceListener/CryptoPriceListener';


/*Database initialization */
import {Database} from "./services/Database";

let database = new Database();
database.init();


/* Routes */

import {ListenerRoutes} from './routes/ListenerRoutes';


const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json())


const PORT = process.env.PORT || 8080;


const BTC = new AltcoinListener('BTC');
BTC.watchForDeposits();

const LTC = new AltcoinListener('LTC');
LTC.watchForDeposits();

const DASH = new AltcoinListener('DASH');
DASH.watchForDeposits();

const ZEC = new AltcoinListener('ZEC');
ZEC.watchForDeposits();

const DOGE = new AltcoinListener('DOGE');
DOGE.watchForDeposits();

// const BCH = new AltcoinListener('BCH');
// BCH.watchForDeposits();

const ETH = new ETHListener();
ETH.watchForDeposits();


const internalListener = new InternalListener();
internalListener.watchForAllTxs();

const membershipExpirationListener = new MembershipExpirationListener();
membershipExpirationListener.watchForExpiredMemberships();

const cryptoPriceListener = new CryptoPriceListener();
cryptoPriceListener.watchForPrices();


app.get('/',(req, res)=>{
	res.send('Hello World');
});

app.use('/altcoin', ListenerRoutes);



app.listen(PORT,()=>{
	console.log(`Node Server initialized on http://localhost:${PORT}`);
});